# Les cellules 
## L'unité du monde vivant
### Les composants d'une cellule :

- noyau
- membrane
- cytoplasme

1. Observe au microscope les différentes cellules
2. Réalise un dessin d'observation
---
![Observation microscopique d'une peau de triton et dessin d'observation](https://sciencesdelavieetdelaterre93.files.wordpress.com/2015/09/cellules-grenouille.jpg)
---
<iframe width="1280" height="720" src="https://www.youtube.com/embed/xTP62tnYQ4c" title="Comment utiliser un microscope optique ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
----
<iframe name="vidéo" src="https://cdn.reseau-canope.fr/medias/corpus/message_nerveux_college-HD.mp4" scrolling="yes" height="220" width="180" FRAMEBORDER="yes"></iframe>
